<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>




<?php if ( is_page_template( 'page-templates/work.php' ) ) { ?>
<div class="wrapper pie section">
<?php } else { ?>
	<div class="wrapper pie footer">
<?php } ?>

	<footer>
		<nav id="site-navigation" class="main-navigation footer-flex footer-flex1" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'footer-menu', ) ); ?>
		</nav><!-- .main-navigation -->		

		<article class="contact-footer footer-flex footer-flex2">
			
			<a href="<?php echo esc_url( home_url( '/about' ) ); ?>" title="About" class="about">About</a>
			<div style="clear:both;"></div>
			<div class="social--container">
				<p>
					<a href="mailto:<?php the_field('email','option'); ?>" title="Write me"><?php the_field('email','option'); ?></a>
					<a href="tel:<?php the_field('telefono','option'); ?>" title="Talk me"><?php the_field('telefono','option'); ?></a>
					<?php the_field('direccion','option'); ?>
				</p>
			
				
				<?php if(get_field('redes_sociales','option')): ?>
					<ul class="rrss footer-flex footer-flex2">
						<?php while(has_sub_field('redes_sociales','option')): ?>
							<li><a href="<?php the_sub_field('enlace','option'); ?>" title="<?php the_sub_field('nombre','option'); ?>"><?php the_sub_field('nombre','option'); ?></a></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				
			</div>
		
		
		</article>

		<article class="txt-footer footer-flex footer-flex3">
			<?php the_field('texto_footer','option'); ?>
			<?php the_field('copyright','option'); ?>
		</article>
		
	</footer>
</div>

<?php if ( is_page_template( 'page-templates/work.php' ) ) { ?>
</div>	
   </div>
<?php } else { ?>
	
<?php } ?>



<!--<script type="text/javascript" src="http://www.fvidal1up.es.mialias.net/andrea/wp-content/themes/twentysixteen-child/js/vendors/scrolloverflow.js"></script>
<script type="text/javascript" src="http://www.fvidal1up.es.mialias.net/andrea/wp-content/themes/twentysixteen-child/js/fullpage.js"></script>
<script type="text/javascript" src="http://www.fvidal1up.es.mialias.net/andrea/wp-content/themes/twentysixteen-child/js/examples.js"></script>
<script type="text/javascript">
    var myFullpage = new fullpage('#fullpage', {
        scrollingSpeed: 1000,
        scrollOverflow: true,
        scrollBar: true
    });
</script>-->





<?php wp_footer(); ?>
</body>
</html>
