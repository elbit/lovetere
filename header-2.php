<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://www.andrealovetere.com/wp-content/themes/twentysixteen-child/css/font-awesome.css">
	<link rel="stylesheet" href="https://www.andrealovetere.com/wp-content/themes/twentysixteen-child/css/style2.css">

	
	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	
	
</head>

<body id="hola" <?php body_class(); ?>>


<div id="fullpage" class="fullpage-wrapper">


<div class="section fp-section-footer fp-section fp-table" data-fp-styles="null" >
	<div class="fp-tableCell">
	
	<header id="cabecera">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="Home" class="logo">
			<img src="https://www.staging2.andrealovetere.com/wp-content/themes/twentysixteen-child/images/Logo.svg" alt="Andre Lo Ventere"/>
		</a>
		
		<a href="<?php echo esc_url( home_url( '/about' ) ); ?>" title="About" class="about">
			About
		</a>
		
		<?php if(get_field('redes_sociales','option')): ?>
			<ul class="rrss">
				<?php while(has_sub_field('redes_sociales','option')): ?>
					<li><a href="<?php the_sub_field('enlace','option'); ?>" title="<?php the_sub_field('nombre','option'); ?>" target="blank"><?php the_sub_field('nombre','option'); ?></a></li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		
		<article>
			<a href="mailto:<?php the_field('email','option'); ?>" title="Write me"><?php the_field('email','option'); ?></a>
			<a href="tel:<?php the_field('telefono','option'); ?>" title="Talk me"><?php the_field('telefono','option'); ?></a>
			<p><?php the_field('direccion','option'); ?></p>
		</article>
	
		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
			
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'primary-menu', ) ); ?>
		</nav><!-- .main-navigation -->
		
	</header><!-- .site-header -->
</div>
</div>

