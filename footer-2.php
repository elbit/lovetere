<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>




<div class="wrapper pie section fp-section-footer fp-section fp-table">


	<footer>
		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'footer-menu', ) ); ?>
		</nav><!-- .main-navigation -->		

		<article class="contact-footer">
			
			<a href="<?php echo esc_url( home_url( '/about' ) ); ?>" title="About" class="about">About</a>
			<div style="clear:both;"></div>
			<p>
				<a href="mailto:<?php the_field('email','option'); ?>" title="Write me"><?php the_field('email','option'); ?></a>
				<a href="tel:<?php the_field('telefono','option'); ?>" title="Talk me"><?php the_field('telefono','option'); ?></a>
				<?php the_field('direccion','option'); ?>
			</p>
		
		
		<?php if(get_field('redes_sociales','option')): ?>
			<ul class="rrss">
				<?php while(has_sub_field('redes_sociales','option')): ?>
					<li><a href="<?php the_sub_field('enlace','option'); ?>" title="<?php the_sub_field('nombre','option'); ?>"><?php the_sub_field('nombre','option'); ?></a></li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		
		</article>
		
		<article class="txt-footer">
			<?php the_field('texto_footer','option'); ?>
			
			<?php the_field('copyright','option'); ?>
		</article>
		
	</footer>
</div>

	

	
<div id="overlay" class="fixed hidden pin z-10"></div>


</div>

<a href="#hola" title="Back to top" id="smoothup">^Top</a>	

<?php wp_footer(); ?>
</body>
</html>
