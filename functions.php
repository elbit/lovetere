<?php


/*ADVANCE CUSTOM TYPE OPTIONS*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Configuraci&oacute;n',
		'menu_title'	=> 'Configuraci&oacute;n',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


?>