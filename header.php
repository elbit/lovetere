<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- <link rel="stylesheet" href="http://www,andrealovetere.com/wp-content/themes/twentysixteen-child/css/font-awesome.css"> bug or disabled ?-->
	
	<!-- <link rel="stylesheet" type="text/css" href="http://www,andrealovetere.com/wp-content/themes/twentysixteen-child/css/fullpage.css" /> -->  <!-- bug or disabled ?-->
	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	
	<script src="https://apis.google.com/js/platform.js" async defer></script> <!-- bit- for what is used? -->
	
	<!--<script src="http://localhost/andrealovetere/wp-content/themes/twentysixteen-child/js/scroll-entrance.js"></script>-->
	
	
</head>

<body id="hola" <?php body_class(); ?>>

<!--<div id="over">
	<img src="http://localhost/andrealovetere/wp-content/themes/twentysixteen-child/images/advertising02.png"/>
</div>-->

	<button class="bit-rwd-menu_switch">Menú</button>

    <header id="absolute-layout" class="bit-rwd-menu"><!--  why this id, no BEM wtf? -->
		
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="Home" class="new_logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg"  alt="Andrea LoVetere"/>
					<!-- <span>Andrea Lovetere <br> Design & Art Direction</span> -->
				</a>
				
				<a href="<?php echo esc_url( home_url( '/about' ) ); ?>" title="About" class="new-about">
					About
				</a>
			
				<?php if(get_field('redes_sociales','option')): ?>
				<ul class="rrss">
					<?php while(has_sub_field('redes_sociales','option')): ?>
					<li><a href="<?php the_sub_field('enlace','option'); ?>" title="<?php the_sub_field('nombre','option'); ?>" target="blank"><?php the_sub_field('nombre','option'); ?></a></li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			
				<div class="address">
					<a href="mailto:<?php the_field('email','option'); ?>" title="Write me"><?php the_field('email','option'); ?></a>
					<a href="tel:<?php the_field('telefono','option'); ?>" title="Talk me"><?php the_field('telefono','option'); ?></a>
					<p><?php the_field('direccion','option'); ?></p>
				</div>
		
				<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'primary-menu', ) ); ?>
				</nav><!-- .main-navigation -->

				<header class="titulo-works"><h1><?php the_field('titulo_works'); ?></h1></header>

                		           						
			
		
	</header><!-- .site-header -->

