<?php
/**
 * Template Name: TEXTOS LEGALES
 */

get_header(); ?>


<section class="contenido">

	<h2><?php the_title(); ?></h2>

	<?php the_field('texto-legal'); ?>
	

</section>


<?php get_footer(); ?>