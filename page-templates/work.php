<?php
/**
 * Template Name: WORK
 */

get_header(); ?>

<div class="wrapper" >

	<h1>This is work temp</h1>
	
	<h1 class="mob-show"><?php the_title(); ?></h1>

	<figure> <!--bit - why figure? look for img size, get thumnails or srcset -->
		<img src="<?php the_field('imagen_destacada'); ?>" alt="<?php the_title();?>" width="" height=""/>
	</figure>

</div>	

</div> <!-- bit - What is closing this? -->

<div class="wrapper "> <!-- Descripción -->
	<section>
		<h1 class="mob-hide"><?php the_title();?></h1>	
		
		<article class="txt-project">

			<h2><?php the_field('titulo_del_texto'); ?></h2>

			<?php the_field('texto_del_proyecto'); ?>

			<ul>
				<li><strong>Role</strong><span><?php the_field('role'); ?></span></li>
				<li><strong>Team</strong><span><?php the_field('work'); ?></span></li>
				<li><strong>Output</strong><span><?php the_field('output'); ?></span></li>
			</ul>

		</article>
		
	</section>
</div>

<div class="mobile-padding-wrapper">
	<?php if(get_field('imagenes_del_proyecto')): ?>
		
		<?php while(has_sub_field('imagenes_del_proyecto')): ?>
			
			<!-- bit- why this component is used for rwd, better way, IMP -->
			
			<?php if( get_sub_field('imagen_o_video') == 'imagen' ){ ?>
				<figure style="background:url('<?php the_sub_field('imagen_del_proyecto'); ?>')" class="wrapper images-works section">
					<img src="<?php the_sub_field('imagen_del_proyecto'); ?>" alt="" width="" height="" class="w-full h-full object-cover"/>
				</figure>
			<?php } else { ?>
		
					<?php while(has_sub_field('video')): ?>
						<video poster="<?php the_sub_field('imagen_video'); ?>" controls>
							<source src="<?php the_sub_field('video_mp4'); ?>" type="video/mp4">
							<source src="<?php the_sub_field('video_ogg'); ?>" type="video/ogg">
							<source src="<?php the_sub_field('video_webm'); ?>" type="video/webm">
						</video>		
					<?php endwhile; ?>
				
			<?php } ?>	
		<?php endwhile; ?>
	<?php endif; ?>
</div>	
	
	 <script> //bit this is repeated?
		jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() < 800) {
            $('#smoothup') .fadeOut(1500);
        } else {
            $('#smoothup') .fadeIn(1500);
        }
        
        if ($(this).scrollTop() < 800) {
            $('#smoothup2') .fadeOut(1500);
        } else {
            $('#smoothup2') .fadeIn(1500);
        }
    });
    $('#smoothup').on('click', function(){
        $('html, body').animate({scrollTop:0}, 'fast');
        return false;
        });
});
	</script>
	
<a href="#hola" title="Back to top" id="smoothup">^Top</a>


<?php global $post;
  if ( $post->post_parent ) { ?>
    <a href="<?php echo get_permalink( $post->post_parent ); ?>" title="Back" id="smoothup2"> </a>
<?php } ?>

	
<?php get_footer(); ?>