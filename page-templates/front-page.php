<?php


get_header(); ?>


<div>
	
	
</div>



<section class="wrapper" id="productos-home" >

	

	<?php $count = 0; ?>

	<?php if(get_field('works1')): ?>
		<?php while(has_sub_field('works1')): ?>
			<a href="<?php the_sub_field('enlace'); ?>" title="Ir a <?php the_sub_field('titulo'); ?>" class="works1-<?php $count;?>">
				<figure><img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/></figure>
				<article>
					<h4><?php the_sub_field('titulo'); ?></h4>
				</article>
			</a>
			<?php $count++;?>
		<?php endwhile; ?>
	<?php endif; ?>
</section>


<section>
	

<?php $count = 0; ?>
	
<?php if(get_field('bloques_contenido')): ?>
	<?php while(has_sub_field('bloques_contenido')): ?>
		<?php if ($count == 0 || $count == 1) { ?>
				<article class="bloque" >
		<?php } else { ?>
			<article class="bloque" data-entrance="fade">
		<?php } ?>				
					<figure>
						<img src="<?php the_sub_field('icono'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/>
					</figure>
					<div>
						<h2><?php the_sub_field('titulo'); ?></h2>
						<?php the_sub_field('texto'); ?>
						<a href="<?php the_sub_field('enlace_boton'); ?>" title="<?php the_sub_field('texto_boton'); ?>">
							<?php the_sub_field('texto_boton'); ?>
						</a>
					</div>
					
				</article>
				<?php $count++;?>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>

</section>


<?php get_footer(); ?>