<?php
/**
 * Template Name: EJEMPLO
 */

get_header(2); ?>

<div class="section relative overflow-hidden antialiased fp-section fp-table" data-fp-styles="null" >
	<div class="fp-tableCell">	
		<?php if( get_field('imagen_o_video') == 'imagen' ){ ?>
		
			<figure>
				<img src="<?php the_field('imagen_destacada'); ?>" alt="<?php the_title();?>" width="" height=""/>
			</figure>
			
		<?php } else { ?>
			
				<?php while(has_sub_field('video')): ?>
					<video poster="<?php the_sub_field('imagen_video'); ?>" controls>
						<source src="<?php the_sub_field('video_mp4'); ?>" type="video/mp4">
						<source src="<?php the_sub_field('video_ogg'); ?>" type="video/ogg">
						<source src="<?php the_sub_field('video_webm'); ?>" type="video/webm">
					</video>		
				<?php endwhile; ?>
			
		<?php } ?>
	</div>	
</div>
<div class="section relative overflow-hidden antialiased fp-section fp-table" data-fp-styles="null" >
	<div class="fp-tableCell">
		<h1><?php the_title();?></h1>	
		
		<article class="txt-project">
			<h2><?php the_field('titulo_del_texto'); ?></h2>
			
			<?php the_field('texto_del_proyecto'); ?>
		</article>
		
		<ul>
			<li><strong>Role</strong><span><?php the_field('role'); ?></span></li>
			<li><strong>Team</strong><span><?php the_field('work'); ?></span></li>
			<li><strong>Output</strong><span><?php the_field('output'); ?></span></li>
		</ul>
	</div>
</div>	
			<header id="site-header" class="fixed pin-x pin-t z-50" role="banner" style="display:none;">
				<div id="top-bar" class="container py-4 lg:py-6 flex justify-between items-center transition-2 text-white antialiased leading-none bg-transparent">
					<span class="page-title uppercase flex-1 opacity-0 hidden sm:block text-normal">Home</span>
				</div>

			</header>
			

					
					<?php if(get_field('imagenes_del_proyecto')): ?>
							<?php while(has_sub_field('imagenes_del_proyecto')): ?>
								<div class="section relative overflow-hidden antialiased fp-section fp-table" data-fp-styles="null" >
									<div class="fp-tableCell">
										<img src="<?php the_sub_field('imagen_del_proyecto'); ?>" alt="" width="" height="" class="w-full h-full object-cover"/>
									</div>
								</div>		
							<?php endwhile; ?>
						<?php endif; ?>


	
			

<?php get_footer(2); ?>


<script>
function loadScript(a){var b=document.getElementsByTagName("head")[0],c=document.createElement("script");c.type="text/javascript",c.src="https://tracker.metricool.com/app/resources/be.js",c.onreadystatechange=a,c.onload=a,b.appendChild(c)}loadScript(function(){beTracker.t({hash:'9fcc9aada71055bdd25e15256ebca7ee'})})</script>
<script type="text/javascript" src="https://mesura.eu/content/themes/mesura/dist/main.js?ver=0.2.32"></script>


