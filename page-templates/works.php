<?php
/**
 * Template Name: WORKS
 */

get_header(); ?>

<div class="works--container" >
	
	<div class="works--spacer">&nbsp; </div>

	
	
	<!---------- WORKS 01 ---------->

	<section class="wrapper works"  id="works1">

	<?php $count = 0; ?>

	<?php if(get_field('works1')): ?>
		<?php while(has_sub_field('works1')): ?>

		<!-- 2 lllamadas a la imagen bad , una para mobil como bg otra como img -->
			
			<a href="<?php the_sub_field('enlace'); ?>" title="View <?php the_sub_field('titulo'); ?>" class="works1-<?php echo $count;?>">
				<figure style="background:url('<?php the_sub_field('imagen'); ?>')">
					<img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/></figure>
				<article>
					<h2><?php the_sub_field('titulo'); ?></h2>
				</article>
			</a>
			<?php $count++;?>
		
		<?php endwhile; ?>
	<?php endif; ?>
	</section>


	<!---------- WORKS 02 ---------->

	<section class="wrapper works" id="works2">

	<?php $count = 0; ?>

	<?php if(get_field('works2')): ?>
		<?php while(has_sub_field('works2')): ?>
			<a href="<?php the_sub_field('enlace'); ?>" title="View <?php the_sub_field('titulo'); ?>" class="works2-<?php echo $count;?>">
				<figure style="background:url('<?php //the_sub_field('imagen'); ?>')"><img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/></figure>
				<article>
					<h2><?php the_sub_field('titulo'); ?></h2>
				</article>
			</a>
			<?php $count++;?>
		<?php endwhile; ?>
	<?php endif; ?>
	</section>

	<!---------- WORKS 03 ---------->

	<section class="wrapper works" id="works3">

	<?php $count = 0; ?>

	<?php if(get_field('works3')): ?>
		<?php while(has_sub_field('works3')): ?>
			<a href="<?php the_sub_field('enlace'); ?>" title="View <?php the_sub_field('titulo'); ?>" class="works3-<?php echo $count;?>">
				<figure style="background:url('<?php //the_sub_field('imagen'); ?>')"><img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/></figure>
				<article>
					<h2><?php the_sub_field('titulo'); ?></h2>
				</article>
			</a>
			<?php $count++;?>
		<?php endwhile; ?>
	<?php endif; ?>
	</section>

	<!---------- WORKS 04 ---------->

	<section class="wrapper works" id="works4">

	<?php $count = 0; ?>

	<?php if(get_field('works4')): ?>
		<?php while(has_sub_field('works4')): ?>
			<a href="<?php the_sub_field('enlace'); ?>" title="View <?php the_sub_field('titulo'); ?>" class="works4-<?php echo $count;?>">
				<figure style="background:url('<?php //the_sub_field('imagen'); ?>')"><img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('titulo'); ?>" width="" height=""/></figure>
				<article>>
					<h2><?php the_sub_field('titulo'); ?></h2>
				</article>
			</a>
			<?php $count++;?>
		<?php endwhile; ?>
	<?php endif; ?>
	</section>
</div>




	<script>
		jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() < 800) {
            $('#smoothup') .fadeOut(1500);
        } else {
            $('#smoothup') .fadeIn(1500);
        }
        
        if ($(this).scrollTop() < 800) {
            $('#smoothup2') .fadeOut(1500);
        } else {
            $('#smoothup2') .fadeIn(1500);
        }
    });
    $('#smoothup').on('click', function(){
        $('html, body').animate({scrollTop:0}, 'fast');
        return false;
        });
});
	</script>
	
<a href="#hola" title="Back to top" id="smoothup">^Top</a>


    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Back" id="smoothup2"></a>


<?php get_footer(); ?>