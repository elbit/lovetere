<?php
/**
 * Template Name: ABOUT
 */

get_header(); ?>

<div class="wrapper about-page">

	<header class="about--header">
		<h1><?php the_title();?></h1>
	</header>

	<section>
	
		<article class="txt-about">
			<?php the_field('texto_1'); ?>
		
			<?php the_field('texto_2'); ?>
		
			<?php the_field('texto_3'); ?>
		</article>
		
		<!-- bit- why bg img on figure? -->
		<figure class="andrea-image" style="background:url('<?php the_field('imagen_about'); ?>')">&nbsp;</figure>

		<div class="e-mark-flex-container">
	
			<article class="servicios">
				<h2>Services</h2>
				
				<?php if(get_field('servicios')): ?>
					<ul>
						<?php while(has_sub_field('servicios')): ?>
							<li><?php the_sub_field('servicio'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</article>
		
			<article class="servicios">
				<h2>Clients</h2>
				
				<?php if(get_field('clientes')): ?>
					<ul>
						<?php while(has_sub_field('clientes')): ?>
							<li><?php the_sub_field('cliente'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</article>
			
			<div style="clear:both;"></div>
			
			<article class="servicios">
				<h2>Recognitions</h2>
				
				<?php if(get_field('reconocimientos')): ?>
					<ul>
						<?php while(has_sub_field('reconocimientos')): ?>
							<li><?php the_sub_field('reconocimiento'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</article>
			
			<article class="servicios">
				<h2>Collaborations</h2>
				
				<?php if(get_field('colaboraciones')): ?>
					<ul>
						<?php while(has_sub_field('colaboraciones')): ?>
							<li><?php the_sub_field('colaboracion'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</article>

		</div>
		
	</section>
</div>

<?php get_footer(); ?>